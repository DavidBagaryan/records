from django.apps import AppConfig


class DwhConfig(AppConfig):
    name = 'dwh'
