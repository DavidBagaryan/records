from django.db import models


class Record(models.Model):
    title = models.CharField(max_length=150, db_index=True)
