from rest_framework import serializers

from .models import *


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ('_id', 'title')

    _id = serializers.IntegerField(source='id')
