from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework import generics
from rest_framework.response import Response

from .serializers import *


class BaseRecordView:
    queryset = Record.objects.all()
    serializer_class = RecordSerializer


class RecordListView(BaseRecordView, generics.UpdateAPIView, generics.ListCreateAPIView):
    def put(self, request, *args, **kwargs):
        record_id = request.data.get('_id')
        title = request.data.get('title')

        if record_id is None or record_id == '':
            return HttpResponseRedirect(reverse('dwh:records_list'))

        record = generics.get_object_or_404(self.queryset, id=record_id)
        record.id = record_id
        record.title = title
        record.save()

        return Response(record, status=200)


class RecordDetailView(BaseRecordView, generics.RetrieveUpdateDestroyAPIView):
    pass
