#!/bin/sh

cd ./endpoints/

./manage.py makemigrations
./manage.py migrate

echo "from django.contrib.auth.models import User;" \
     "User.objects.all().delete();" \
     " User.objects.create_superuser('admin', 'admin@example.com', '321456')" | ./manage.py shell

exec "$@"
